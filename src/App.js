import Botonera from "./components/Botonera";
import { BrowserRouter as Router, Route } from "react-router-dom";

function App() {
  return (
    <>
      <Router>
        <Route path="/:section" component={Botonera} />
        <Route exact path="/" component={Botonera} />
      </Router>
    </>
  );
}

export default App;
